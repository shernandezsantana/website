www.senaidahernandez.com
========

Sources for my website [http://senaidahernandez.com/](http://senaidahernandez.com/).

Content is licensed under [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/). Code is licensed under [GNU Public License v3.0](https://www.gnu.org/copyleft/gpl.html).

This site is generated by [Pelican](https://github.com/getpelican/pelican), a static site generator that supports Markdown and reST syntax. The theme is an extension of [tuxlite_tbs](https://github.com/getpelican/pelican-themes/tree/master/tuxlite_tbs).

# How did I create my site with Pelican?

First I installed Pelican and generated the site via the command:
`pelican-quickstart`

This generated several files and folders:
 - *pelicaconf.py*, which contains the settings to generate website (sitename, links, selected theme, plugins).
 - *publishconf.py*, which contains the settings to generate and send the site to the website server.
 - *makefile*, which has all the command necessary for the generating and sending the website, including the details of the server.
 - *content*, which contains the markdown files and the images you want to use on the website.
 - *output*, where the html files generated by pelican will appear every time you generate the site. These files are the ones the website provider needs and the ones that pelican uploads.

Additionally, I used part of the source code from another pelican-based website as a reference. In particular, I copied the file *pelicaconf.py*.

Then, I included the markdown files and I choose a theme, which I later modified to meet my needs.

As a last step, I uploaded the files via the makefile from the *shell*.

# What did I learn?

- Modifying a theme is tricky. If you want to mofify anything, you need to find the function that you need. That may take a while and it really depends on the theme that you choose to start with. Some may have nice commenting, some not. Some may include all possible functions you may want to modify, some not.
- Your website provider may not let you upload via *ssh* if you don't pay for it. In my case, they only allow *ssh* to *vps* users, who pay for access to a virtual machine (*vps*: virtual private server). Instead, I use ftp or WebDAV.
- If you have *makefile*, you can do everything that you need from the commands set in it (generating, serving locally, uploading, ...).
- When uploading via *makefile*, you may have problems with the certificate. See possible solutions [here](https://github.com/getpelican/pelican/issues/444).
- I really like Pelican. Once you set up your theme, it really makes things easy.
