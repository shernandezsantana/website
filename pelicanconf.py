#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Senaida Hernández Santana'
SITENAME = 'Senaida Hernández Santana'
SITEURL = 'http://senaidahernandez.com'
DELETE_OUTPUT_DIRECTORY = False
RELATIVE_URLS = True

PATH = 'content'
TIMEZONE = 'Europe/Madrid'
DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = None #'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Social widget
SOCIAL = (
	  ('Email','https://spamty.eu/mail/v4/1724/EeUOjISMCoc02d8f82/'),
          ('LinkedIn', 'https://www.linkedin.com/in/senaidahernandez/'),
          ('GitLab', 'https://gitlab.com/senaidahernandez'),
         ('Google Scholar', 'https://scholar.google.com/citations?user=vfmXyZUAAAAJ'),
          )

MARKUP = ('md', 'ipynb')
DEFAULT_PAGINATION = 1000
DISPLAY_CATEGORIES_ON_MENU = False
ARTICLE_ORDER_BY = 'reversed-date'
PAGE_ORDER_BY = 'order'
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
#INDEX_SAVE_AS = 'blog.html'
STATIC_PATHS = ['documents','images', 'static']
EXTRA_PATH_METADATA = {
    'static/.htaccess': {'path': '.htaccess'},
    }
AUTHOR_SAVE_AS = ''
#TYPOGRIFY = False
#TYPOGRIFY_IGNORE_TAGS = ['a']

# Plugin-related settings
PLUGIN_PATHS = ['plugins']
IPYNB_USE_META_SUMMARY = True
PLUGINS = ['feed_summary', 'render_math', 'sitemap']
FEED_USE_SUMMARY = True
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}


# Theme-related settings 
THEME = 'themes/tuxlite_tbs_myown'
#THEME = 'themes/new-bootstrap2'
