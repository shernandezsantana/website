Title: Home
Date: 2019-06-21 16:28
Author: Senaida Hernández
Slug: home
order: 0
Summary: Senaida Hernández, research scientist in quantum many-body systems, quantum thermodynamics, and quantum open systems.
URL:
save_as: index.html

<img style="float:left; border: 1px solid black; margin-bottom:4px; margin-right: 20px" src="images/photo.jpg" alt="photo"/>

I am a quantum physicist specialized on Quantum Many-Body Systems, Quantum Thermodynamics, and Quantum Open Systems. I am interested on fundamental problems in a wide range of topics within Quantum Physics, from Quantum Many-Body Sytems and Quantum Thermodynamics to further topics such as Quantum Computing.

I recently obtained my PhD from [ICFO-The Institute of Photonic of Sciences](https://www.icfo.eu/) and the [Polytechnic University of Catalonia](https://www.upc.edu/en). At ICFO, I was a 'laCaixa' - Severo Ochoa Fellow at the [Quantum Information Theory](http://www.icfo.eu/lang/research/groups/groups-details?group_id=19) led by Antonio Acín. During my PhD, I did a discontinous 3-month stay at the Theory Division led by Ignacio Cirac, at [MPQ](http://www2.mpq.mpg.de/Theorygroup/CIRAC/) (Max-Planck Institute for Quantum Optics). These stays were possible thanks to the [Max Planck and Prince of Asturias Mobility Programme](https://www.mpg.de/8034988/Max-Planck_Asturias-Award-Mobility-Programme) and the [Short-term Scientific Mission (STSM)](https://blogs.exeter.ac.uk/qut/stsms-2/), from the COST network Thermodynamics in the Quantum Regime MP1209.

Previously, I completed my Master's thesis on Quantum Optomechanics in 2013, at the [University of La Laguna](https://www.ull.es/en/). I also collaborated in a laser laboratory in 2012 at the University of La Laguna, thanks to the [MEC](http://www.mecd.gob.es/) collaboration fellowship. Additionally I was a summer fellow in 2011 at the [CSIC Institute of Structure of Matter](http://www.iem.csic.es/), thanks to the CSIC program for Introduction to Research (JAE Intro 2010).

<!--  
# Projects

Currently I am involved in several projects connected to ...:

-

# Recent Activities

  - Attending the summer school of HPC in Toronto.
  - Visiting  Perimeter
  - Visit Jens Eisert
  - Visit Nicolas Brunner
  - Research stay
  - Visit
  - Visit

-->
