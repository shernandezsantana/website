Title: Talks
Date: 2019-07-11 19:02
Author: Senaida Hernández
Slug: talks
order: 2

# Talks

## 2019

- [Decay of correlations in long-range interacting systems at non-zero temperature](http://pirsa.org/19020071/), Quantum Discussions at Perimeter Institute. Note: First 15mins of lecture not captured due to a technical error. [![[PDF]](images/pdf.png)](documents/correlationsforlongrange.pdf) [![[video]](images/video_16.png)](http://pirsa.org/index.php?p=media&url=http://pirsa.org/displayFlash.php?id=19020071) [![[audio]](images/audio_16.png)](http://streamer2.perimeterinstitute.ca/mp3/19020071.mp3)
